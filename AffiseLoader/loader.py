import os
import logging
import psycopg2

from pymongo import MongoClient

from api import API
from parser import parse_offer


AFFISE_API_KEY = '1de8abb9f952c2516b998a0c1b8ce00c2a8dbe10'

logger = logging.getLogger('loader')
logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
ch.setFormatter(logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s'))
logger.addHandler(ch)

mongo_client = MongoClient('mongodb://%s:27017/' % os.environ['MONGO_HOST'])
offers_db = mongo_client['offerwall']['offers']
logger.debug('MongoDB connected')

# Temporary "cache" for resolved values of app names
app_dict = {}
# Affise adv id -> adv name
adv_dict = {}
# List of offer ids to be assigned as "top offers"
top_offers = []

with psycopg2.connect(host='bo.datafirst.io', user='admin', password='GoingBack', dbname='stats') as conn:
    cursor = conn.cursor()
    logger.debug('PostgreSQL \'stats\' database connected')

    cursor.execute("SELECT affise_id, title FROM advertisers")
    for adv in cursor.fetchall():
        adv_dict[adv[0]] = adv[1]

    # Instead of doing is_top() for every offer (simultaneously creating too many new connections)
    # it is better to get nessesary ids and do Mongo's "update_many"
    cursor.execute("""SELECT offer_id FROM conversions 
        WHERE created_at::date > current_date - interval '7' 
    GROUP BY offer_id HAVING SUM(revenue-payout) > 50""")
    top_offers = [i[0] for i in cursor.fetchall()]
    logger.info("Count of top offers - %s" % len(top_offers))

affise = API('http://api.datafirst.affise.com/3.0', AFFISE_API_KEY)
affise_offers = affise.get_list('offers', data={
    'status[]': ['active', ],
})
affise_ids = [offer['id'] for offer in affise_offers]
logger.info('Offers from Affise were loaded')


conn = psycopg2.connect(host='bo.datafirst.io', user='application', password='rynqsong', dbname='supremacy')
cursor = conn.cursor()

for offer in affise_offers:
    r = parse_offer(offer)
    r['adv'] = adv_dict[offer['advertiser']]

    if r['app_id'] in app_dict:
        r['app_name'] = app_dict[r['app_id']]
    else:
        app_id = r['app_id']
        if r['os'] == 'iOS':
            app_id = app_id[2:]

        cursor.execute("SELECT title FROM applications WHERE app_id = '%s'" % app_id)
        app_name = cursor.fetchone() or ['']

        r['app_name'] = app_name[0]
        app_dict[r['app_id']] = r['app_name']

    mongo_offer = offers_db.find_one({'id': offer['id']})
    if mongo_offer is None:
        result = offers_db.insert_one(r)
    else:        
        for key in r.keys():
            if key not in mongo_offer.keys() or r[key] != mongo_offer[key]:
                result = offers_db.update_one({'id': offer['id']}, {'$set': {key: r[key]}})
logger.info('MongoDB offers were updated')

result = offers_db.update_many({'id': {'$nin': affise_ids}}, {'$set': {'status': 'paused'}})
logger.info('Mongo offers on pause - %s' % result.modified_count)

result = offers_db.update_many({'id': {'$in': top_offers}}, {'$addToSet': {'tags': 'top'}})
logger.info('Tag "top" added - %s' % result.modified_count)

offers_dict = {}
for pub in affise.ns('admin').get_list('partners'):
    if pub['status'] != 'active':
        continue

    pub_offer_ids = []
    try:
        partner_api = API('http://api.datafirst.affise.com/3.0', pub['api_key']).ns('partner')
        pub_offer_ids = [offer['id'] for offer in partner_api.get_list('offers') if offer['id'] in affise_ids]
    except:
        continue

    for offer_id in pub_offer_ids:
        offers_dict[offer_id] = offers_dict.get(offer_id, []) + [{'id': pub['id'], 'title': pub['login']}, ]

with mongo_client.start_session() as s:
    s.start_transaction()
    # Refresh all pub connections during one atomic operation to provide consistency
    offers_db.update_many({}, {'$set': {'connected_pubs': []}})
    for offer_id, pubs in offers_dict.items():
        result = offers_db.update_one({'id': offer_id}, {'$set': {'connected_pubs': pubs}})

    s.commit_transaction()
conn.close()
