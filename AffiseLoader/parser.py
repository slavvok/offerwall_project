import re
import logging

from datetime import datetime, timedelta

logger = logging.getLogger('loader.parser')

def get_app_info(url):
    os, app_id = '', ''
    try:
        if 'itunes' in url:
            os, app_id = 'iOS', re.search(pattern=r'id([\d]{1,})', string=url).group()
        elif 'play.google' in url:
            os, app_id = 'Android', re.search(pattern=r'(?<=[?&]id=)[^&\n]+', string=url).group()
    finally:
        return os, app_id


def parse_offer(offer):

    os, app_id = get_app_info(offer['preview_url'])
    created_at = datetime.strptime(offer['created_at'], "%Y-%m-%d %H:%M:%S")
    countries = []

    for payment in offer['payments']:
        countries.extend(payment['countries'])
    # To remove dublicates
    countries = list(set(countries))

    pub_caps = {}
    all_caps = aff_caps = 0
    for cap in offer.get('caps', []):
        if cap['affiliate_type'] == 'exact':
            for affiliate in cap['affiliates']:
                pub_caps[str(affiliate)] = cap['value']
        if cap['affiliate_type'] == 'all':
            all_caps = cap['value']

    # Tags
    tags = []
    if datetime.now() - created_at <= timedelta(days=7):
        tags.append('new')
    if '5c1a24e01df25a57498b4771' in offer['categories']:
        tags.append('focus')

    parsed_offer = {
        'id': offer['id'],
        'internal_id': offer['offer_id'],
        'title': offer['title'],
        'created_at': int(created_at.timestamp()),

        'os': os,
        'app_id': app_id,

        'status': offer['status'],
        'privacy': offer['privacy'],
        'tags': tags,
        'countries': countries,

        'all_caps': all_caps,
        'pub_caps': pub_caps,
        'payout': offer['payments'][0]['total'],
        'revenue': offer['payments'][0]['revenue'],
    }

    return parsed_offer
