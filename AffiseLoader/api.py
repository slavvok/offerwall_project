import functools
import time
import requests
import logging

from requests.adapters import HTTPAdapter

MAX_RETRIES = 5
SLEEP_PAUSE = .333

logger = logging.getLogger('loader.AffiseAPI')

def retry(exceptions=(Exception), times=MAX_RETRIES):
    """
    A decorator for retrying a function call with a specified delay in case of a set of exceptions

    :param exceptions:  A tuple of all exceptions that need to be caught for retry
    :param times: no of times the function should be retried
    """
    def outer_wrapper(function):
        @functools.wraps(function)
        def inner_wrapper(*args, **kwargs):
            final_exception = None  
            for _ in range(times):
                try:
                    value = function(*args, **kwargs)
                    return value
                except (exceptions) as e:
                    final_exception = e
                    time.sleep(SLEEP_PAUSE)

            if final_exception is not None:
                raise final_exception
        return inner_wrapper
    return outer_wrapper

class API:
    def __init__(self, url, key, session=None):
        self.url = url
        if self.url[-1] == '/': self.url = self.url[0:-1]

        self.session = session or requests.Session()
        self.session.headers.update({
            'API-Key': key,
            'Content-Type': 'application/x-www-form-urlencoded',
        })
        self.key = key    

    def ns(self, namespace):
        # Returns API object created based on existing
        return API(self.url + '/' + namespace, self.key, self.session)

    @retry(exceptions=(requests.exceptions.ConnectionError), times=MAX_RETRIES)
    def request_page(self, url, params={}):    
        return self.session.get(url=url, params=params).json()

    def get(self, what, id):
        r = self.request_page('%s/%s/%s' % (self.url, what, id))
        return r[what]

    def get_list(self, what, data={}, entity=None):
        entity = entity or what

        data['page'] = 1
        data['limit'] = 500
        what_list = []
        while True:
            r = self.request_page(self.url + '/' + what, params=data)
            if r['status'] != 1:
                logger.error(r)

            what_list.extend(r[entity])
            if 'next_page' in r['pagination'].keys():
                data['page'] += 1
            else:
                break
        logger.debug('Loaded %s - %s' % (entity, len(what_list)))
        return what_list
