import os
import psycopg2
import random

from redis import StrictRedis
from datetime import datetime, timedelta

FORBIDDEN_KEYS = [
    'redis'
]
REDIS_KEY_PATTERN = '{date}_paid_convs_count:{offer}_*'

class App():

    def __init__(self, app):
        self.redis = StrictRedis(host=os.environ['REDIS_HOST'], password=os.environ['REDIS_PASS'], db=1)
        self.app_id = app['_id']
        self.app_name = app['offers'][0].get('app_name', '')
        self.app_status = 'paused'
        self.os = app['offers'][0]['os']

        self.all_caps, self.all_convs = 0, 0

        self.top_payout = 0
        self.countries = []
        self.tags = []
        self.connected_offers = []

        for offer in app['offers']:
            if offer['status'] == 'active':
                self.app_status = 'active'
            if offer['payout'] > self.top_payout:
                self.top_payout = offer['payout']

            pub_convs = self._get_convs(offer['id'])
            self.all_caps += offer['all_caps']

            self.countries.extend(offer['countries'])
            self.tags.extend(offer['tags'])

            self.connected_offers.append({
                'offer_id': offer['id'],
                'offer_name': offer['title'],
                'internal_id': offer['internal_id'],
                'adv': offer.get('adv', ''),

                'payout': offer['payout'],
                'revenue': offer.get('revenue', 0),

                'all_convs': sum(pub_convs.values()),
                'all_caps': offer['all_caps'],
                'pub_convs': pub_convs,
                'pub_caps': offer['pub_caps'],
                'countries': offer['countries'],
                'status': offer['status'],
                'created_at': offer['created_at'],
                'connected_pubs': offer['connected_pubs'],
                'tags': offer['tags'],

                'history': self._get_historical_convs(offer_id=offer['id'], days=7),
            })
            if not self.connected_offers[-1]['all_caps']:
                self.connected_offers[-1]['all_caps'] = sum(self.connected_offers[-1]['pub_caps'].values())


        # If there was no successful response, use longest common substring
        if not self.app_name:
            self.app_name = self.connected_offers[0]['offer_name']
            for offer in self.connected_offers[1:]:
                self.app_name = getSubstring(self.app_name, offer['offer_name'])
            self.app_name = self.app_name.strip()

        self.countries = list(set(self.countries))
        self.tags = list(set(self.tags))
        self.all_convs = sum(i['all_convs'] for i in self.connected_offers)


    def _get_convs(self, offer_id, day=datetime.now()):
        pubs = self.redis.keys(REDIS_KEY_PATTERN.format(date=day.strftime('%Y%m%d'), offer=offer_id))
        pub_dict = {}

        for pub in pubs:
            pub_id = int(pub.decode().split('_')[-1])
            pub_dict[pub_id] = int(self.redis.get(pub.decode()).decode())

        return pub_dict


    def _get_historical_convs(self, offer_id, days=7):
        history = []

        day = datetime.now()
        for _ in range(days):
            day -= timedelta(days=1)
            pub_convs = self._get_convs(offer_id, day=day)
            history.append({
                'date': day.strftime('%Y-%m-%d'),
                'all_convs': sum(pub_convs.values()),
                'pub_convs': pub_convs,
            })

        return history


    def json(self):
        return {x: self.__dict__[x] for x in self.__dict__ if x not in FORBIDDEN_KEYS}


    @staticmethod
    def get_line(i):
        SONG = [
            "Sucker love is heaven sent",
            "You pucker up our passion's spent",
            "My hearts a tart your body's rent",
            "My body's broken yours is bent",
            "Carve your name into my arm",
            "Instead of stressed I lie here charmed",
            "Cause there's nothing else to do",
            "Every me and every you",
            "Sucker love a box I choose",
            "No other box I choose to use",
            "Another love I would abuse",
            "No circumstances could excuse",
            "In the shape of things to come",
            "Too much poison come undone",
            "Cause there's nothing else to do",
            "Every me and every you",
            "Every me and every you",
            "Every me",
            "Sucker love is known to swing",
            "Prone to cling and waste these things",
            "Pucker up for heavens sake",
            "There's never been so much at stake",
            "I serve my head up on a plate",
            "It's only comfort, calling late",
            "Cause there's nothing else to do",
            "Every me and every you",
            "Every me and every you",
            "Every me,",
            "Every me and every you",
            "Every me",
            "Like the naked leads the blind",
            "I know I'm selfish, I'm unkind",
            "Sucker love I always find",
            "Someone to bruise and leave behind",
            "All alone in space and time",
            "There's nothing here but what here's mine",
            "Something borrowed, something blue",
            "Every me and every you",
            "Every me and every you",
            "Every me",
            "Every me and every you",
            "Every me",
        ]
        return SONG[i % 42]


def is_int(val):
    try:
        r = int(val)
        return True
    except:
        return False


def getSubstring(s1, s2):
   m = [[0] * (1 + len(s2)) for i in range(1 + len(s1))]
   longest, x_longest = 0, 0
   for x in range(1, 1 + len(s1)):
       for y in range(1, 1 + len(s2)):
           if s1[x - 1] == s2[y - 1]:
               m[x][y] = m[x - 1][y - 1] + 1
               if m[x][y] > longest:
                   longest = m[x][y]
                   x_longest = x
           else:
               m[x][y] = 0
   return s1[x_longest - longest: x_longest]


def make_array_struct(name, lst):
    query = ''

    for index, d in enumerate(lst):
        if type(d) == str:
            query += '{name}[{index}]={value}&'.format(name=name, index=index, value=d)
        else:
            for key, value in d.items():
                if key.endswith('[]'):
                    key = key.split('[]')[0]
                    template = '{name}[{index}][{key}][]={value}&'
                else:
                    template = '{name}[{index}][{key}]={value}&'

                if type(value) == list:
                    for item in value:
                        query += template.format(
                            name=name, index=index, key=key, value=item)
                else:
                    query += template.format(name=name, index=index, key=key, value=value)
    return query[:-1]