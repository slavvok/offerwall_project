import functools
import mimetypes
import time
import requests

from requests.adapters import HTTPAdapter

MAX_RETRIES = 7
SLEEP_PAUSE = .500

class API:
    def __init__(self, url, key, session=None):
        self.url = url
        if self.url[-1] == '/': self.url = self.url[0:-1]

        self.max_retries = MAX_RETRIES

        self.session = session or requests.Session()
        # self.session.mount('http://', HTTPAdapter(max_retries=7))
        # self.session.mount('https://', HTTPAdapter(max_retries=7))

        self.session.headers.update({
            'API-Key': key,
            'Content-Type': 'application/x-www-form-urlencoded',
        })
        self.key = key    


    def retry(exceptions=(Exception), times=MAX_RETRIES):
        """
        A decorator for retrying a function call with a specified delay in case of a set of exceptions

        :param exceptions:  A tuple of all exceptions that need to be caught for retry
        :param times: no of times the function should be retried
        """
        def outer_wrapper(function):
            @functools.wraps(function)
            def inner_wrapper(*args, **kwargs):
                final_exception = None  
                for counter in range(times):
                    try:
                        value = function(*args, **kwargs)
                        return value
                    except (exceptions) as e:
                        final_exception = e
                        time.sleep(SLEEP_PAUSE)

                if final_exception is not None:
                    raise final_exception
            return inner_wrapper
        return outer_wrapper


    @retry(exceptions=(requests.exceptions.ConnectionError), times=MAX_RETRIES)
    def request_page(self, url, params={}):    
        return self.session.get(url=url, params=params).json()

    @retry(exceptions=(requests.exceptions.ConnectionError), times=MAX_RETRIES)
    def post_request(self, url, data={}):    
        return self.session.post(url=url, data=data).json()


    def ns(self, namespace):
        """ Returns API object created based on existing
        """
        return API(self.url + '/' + namespace, self.key, self.session)


    def get(self, what, id):
        """ Returns specified object

        Args:
            what: Name of object instance
            id: Integer ID
        """
        r = self.request_page(self.url+'/'+what+'/'+str(id))
        return r[what]


    def get_list(self, what, data={}, entity=None):
        """ Returns list of objects

        Args:
            what: URL ending
            data: Query params for GET request
            entity: Name of list of objects. Equal to 'what' by default
                Can be changed because 'stats/custom' returns list named 'stats'
        """
        entity = entity or what

        data['page'] = 1
        data['limit'] = 400
        what_list = []
        while True:
            r = self.request_page(self.url + '/' + what, params=data)
            what_list.extend(r[entity])

            if 'next_page' in r['pagination'].keys():
                data['page'] += 1
            else:
                break
        return what_list

    def get_page(self, what, data={}, page=1, limit=200, entity=None):
        entity = entity or what

        data['page'] = page
        data['limit'] = limit
        
        r = self.request_page(self.url + '/' + what, params=data)
        if r['status'] != 1:
            raise Exception(r['message'])
        if (page-1) * limit > r['pagination']['total_count']:
            return None, None
        return r[entity], r['pagination']['total_count']
    

    def edit(self, what, id, data={}):
        """ TODO: Make proper docstring
        """
        r = self.session.post(self.url+'/'+what+'/'+str(id), data=data).json()
        return r
    
    def mass_edit(self, what, data={}):
        r = self.session.post(self.url + '/' + what, data=data).json()
        return r
