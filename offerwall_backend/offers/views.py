import os
import requests
import psycopg2

from django.http import HttpResponse, JsonResponse, HttpResponseBadRequest
from django.template import loader
from django.shortcuts import render
from django.contrib.auth import authenticate, login

from redis import StrictRedis
from pymongo import MongoClient
from datetime import datetime

from .utils import App, is_int, make_array_struct
from .api import API


def search(request):
    redis = StrictRedis(host=os.environ['REDIS_HOST'], password=os.environ['REDIS_PASS'], db=1)
    mongo_client = MongoClient('mongodb://%s:27017/' % os.environ['MONGO_HOST'])
    offers_db = mongo_client['offerwall']['offers']

    page = int(request.GET.get('page', 1))
    limit = 50

    filters = {}
    query = [
        {
            '$match': {
                'os': {'$ne': ''},
            },
        },
        {
            '$group': {
                '_id': '$app_id',
                'max_date': {'$max': '$created_at'},
                'offers': {'$push': '$$ROOT'},
            }
        },
    ]

    if request.GET.get('offer_id', '') and is_int(request.GET['offer_id']):
        filters['offer_id'] = int(request.GET['offer_id'])
        query[0]['$match']['id'] = {'$eq': filters['offer_id']}

    if request.GET.get('os', ''):
        filters['os'] = request.GET['os']
        query[0]['$match']['os']['$eq'] = filters['os']

    if request.GET.get('app', ''):
        filters['app'] = request.GET['app']
        query[0]['$match']['$text'] = {'$search': filters['app']}

    if request.GET.get('countries', ''):
        r = requests.get('https://raw.githubusercontent.com/Miguel-Frazao/world-data/master/countries.json').json()
        filters['countries'] = [i.lower() for i in request.GET['countries'].split(',')]
        query[0]['$match']['countries'] = {'$in': filters['countries']}

    if request.GET.get('tag', ''):
        filters['tags'] = [request.GET['tag'], ]
        query[0]['$match']['tags'] = {'$in': filters['tags']}

    if request.GET.get('adv', ''):
        filters['adv'] = request.GET['adv']
        query[0]['$match']['adv'] = {'$regex': filters['adv'], '$options': 'i'}

    if request.GET.get('page', ''):
        page = int(request.GET['page'])

    print (filters)

    # Sorting
    query.append({'$sort': {'max_date': -1}})

    # Pagination
    query.append({'$skip': (page-1)*limit})
    query.append({'$limit': limit})

    offers_by_app = [App(app).json() for app in offers_db.aggregate(query)]

    for offer in offers_by_app:
        print ('\n\n%s\n\n' % offer)

    return JsonResponse({
        'data': offers_by_app,
        'pagination': {
            'page': page,
            'limit': limit,
            # 'total_count': len(offers_db.distinct('app_id')),
        },
        'filters': filters,
    })

def offerwall(request):
    return render(request, 'index.html', {})

def get_advertisers(request):
    client = psycopg2.connect(dsn="user=admin password=GoingBack dbname=stats host=bo.datafirst.io port=5432")
    cursor = client.cursor()

    cursor.execute("SELECT title FROM advertisers ORDER BY title")
    r = [i[0] for i in cursor.fetchall()]
    client.close()
    return JsonResponse({'data': r}, safe=False)

def get_countries(request):
    r = requests.get('https://raw.githubusercontent.com/Miguel-Frazao/world-data/master/countries.json').json()
    return JsonResponse({
        'data': [{'name': i['name'], 'code': i['code_iso2'].lower()} for i in r],
    })


def disable_publisher(request):
    offer_id = request.GET.get('offer_id')
    pub_id = request.GET.get('pub_id')

    if not is_int(offer_id) or not is_int(pub_id):
        return HttpResponseBadRequest()

    offer_id = int(offer_id)
    pub_id = int(pub_id)

    affise = API('http://api.datafirst.affise.com/3.0', '1de8abb9f952c2516b998a0c1b8ce00c2a8dbe10')
    r = affise.ns('offer').mass_edit('disable-affiliate', data={
        'offer_id': offer_id, 'pid': pub_id,
    })

    if r['status'] == 1:
        mongo_client = MongoClient('mongodb://localhost:27017/')
        mongo_client['offerwall']['offers'].update_one({'id': offer_id}, {'$pull': {'connected_pubs': pub_id}})

    return JsonResponse(r)

def enable_publisher(request):
    offer_id = request.GET.get('offer_id')
    pub_id = request.GET.get('pub_id')
    # Optional
    caps = request.GET.get('caps')

    if not is_int(offer_id) or not is_int(pub_id):
        return HttpResponseBadRequest()
    if caps is not None and not is_int(caps):
        return HttpResponseBadRequest()

    offer_id = int(offer_id)
    pub_id = int(pub_id)

    mongo_client = MongoClient('mongodb://localhost:27017/')
    affise = API('http://api.datafirst.affise.com/3.0', '1de8abb9f952c2516b998a0c1b8ce00c2a8dbe10')

    r = affise.ns('offer').mass_edit('enable-affiliate', data={
        'offer_id': offer_id, 'pid': pub_id,
    })

    if r['status'] == 1:
        # TODO: fix (not) adding pub to 'connected_pubs'
        # Task for future commits. Right now should be solved by loader's frequent updates
        result = mongo_client['offerwall']['offers'].update_one({'id': offer_id}, {'$addToSet': {'connected_pubs': pub_id}})

    if caps is not None:
        new_caps = int(caps)
        offer = affise.get('offer', offer_id)

        cap_items = []
        affiliates = []

        for cap in offer.get('caps', []):
            if cap['affiliate_type'] == 'all':
                cap_items.append({
                    'period': cap['period'], 'goal_type': cap['goal_type'], 'type': cap['type'],
                    'goals[]': list(cap['goals'].keys()),
                    'value': cap['value'], 'affiliate_type': 'all',
                })
            if cap['affiliate_type'] == 'exact':
                affiliates.extend(cap['affiliates'])
                cap_items.append({
                    'period': cap['period'], 'goal_type': cap['goal_type'], 'type': cap['type'],
                    'goals[]': list(cap['goals'].keys()), 'affiliates[]': cap['affiliates'],
                    'value': cap['value'], 'affiliate_type': 'exact',
                })
                if pub_id in cap['affiliates']:
                    cap_items[-1]['value'] = new_caps

        if pub_id not in affiliates:
            cap_items.append({
                'period': 'day', 'goal_type': 'exact', 'goals[]': ['1'], 'type': 'conversions',
                'value': new_caps, 'affiliate_type': 'exact', 'affiliates[]': pub_id,
            })

        result = affise.ns('admin').edit('offer', offer_id, make_array_struct(name='caps', lst=cap_items))
        if result['status'] == 1:
            mongo_client['offerwall']['offers'].update_one({'id': offer_id}, {'$set': {'pub_caps.%s' % pub_id: new_caps}})
        else:
            r['message'] += '. %s' % r['message']

    return JsonResponse(r)


def change_caps(request):
    offer_id = request.GET.get('offer_id')
    pub_id = request.GET.get('pub_id')
    caps = request.GET.get('caps')

    if not is_int(offer_id) or not is_int(pub_id) or not is_int(caps):
        return HttpResponseBadRequest()

    offer_id = int(offer_id)
    pub_id = int(pub_id)
    new_caps = int(caps)

    mongo_client = MongoClient('mongodb://%s:27017/' % os.environ['MONGO_HOST'])
    affise = API('http://api.datafirst.affise.com/3.0', '1de8abb9f952c2516b998a0c1b8ce00c2a8dbe10')

    offer = affise.get('offer', offer_id)

    cap_items = []
    affiliates = []

    for cap in offer.get('caps', []):
        if cap['affiliate_type'] == 'all':
            cap_items.append({
                'period': cap['period'], 'goal_type': cap['goal_type'], 'type': cap['type'],
                'goals[]': list(cap['goals'].keys()),
                'value': cap['value'], 'affiliate_type': 'all',
            })
        if cap['affiliate_type'] == 'exact':
            affiliates.extend(cap['affiliates'])
            cap_items.append({
                'period': cap['period'], 'goal_type': cap['goal_type'], 'type': cap['type'],
                'goals[]': list(cap['goals'].keys()), 'affiliates[]': cap['affiliates'],
                'value': cap['value'], 'affiliate_type': 'exact',
            })
            if pub_id in cap['affiliates']:
                cap_items[-1]['value'] = new_caps

    if pub_id not in affiliates:
        cap_items.append({
            'period': 'day', 'goal_type': 'exact', 'goals[]': ['1'], 'type': 'conversions',
            'value': new_caps, 'affiliate_type': 'exact', 'affiliates[]': pub_id,
        })

    r = affise.ns('admin').edit('offer', offer_id, make_array_struct(name='caps', lst=cap_items))
    if r['status'] == 1:
        mongo_client['offerwall']['offers'].update_one({'id': offer_id}, {'$set': {'pub_caps.%s' % pub_id: new_caps}})
        del r['offer']
        r['message'] = 'Cap was changed successfully'

    return JsonResponse(r)
