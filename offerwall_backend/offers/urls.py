from django.urls import path

from . import views

urlpatterns = [
    path('', views.offerwall, name='profile'),
    # path('get_data', views.get_data, name='get_data'),
    # path('get_pubs', views.get_pubs, name='get_pubs'),

    path('get_countries', views.get_countries, name='get_countries'),
    path('get_advertisers', views.get_advertisers, name='get_advertisers'),

    path('disable_publisher', views.disable_publisher, name='disable_publisher'),
    path('enable_publisher', views.enable_publisher, name='enable_publisher'),
    path('change_caps', views.change_caps, name='change_caps'),

    path('search', views.search, name='search')
]
