ifeq ($(PRODUCTION),TRUE)
	DC_CMD = docker-compose -f docker-compose.yml -f docker-compose.prod.yml
else
	DC_CMD = docker-compose
endif

rmpyc:
	find . -name '*.pyc' -delete -print
	
psync:
	pip install --upgrade pip && pip install --upgrade pip-tools && pip-sync app/requirements.txt

mongo:
	docker-compose exec mongodb mongo

redis:
	docker-compose exec redis redis-cli

logs:
	$(DC_CMD) logs --follow --tail=1000 --timestamps ${arg}

rebuild:
	$(DC_CMD) config
	$(DC_CMD) build
	$(DC_CMD) down
	$(DC_CMD) up -d

down:
	$(DC_CMD) down

ps:
	$(DC_CMD) ps

envtest:
	@echo PRODUCTION: $(PRODUCTION)
	@echo DOCKER-COMPOSE: $(DC_CMD)
